Pelee in keras 
======================

Implementation of the Pelee model (https://arxiv.org/abs/1804.06882) in Keras

This project utilizes SSD implementation from: https://github.com/pierluigiferrari/ssd_keras.git
I have forked it to be sure that code version is static.

The purpose of this project is to implement Pelee model for object detection in keras library.
I managed to find some implementations of PeleeNet (classification model) in keras and implementation of pelee in caffe.
But as far as I know there is no implementation of Pelee in keras in the open access.
I hope this repo will be helpful for someone.

Quickstart
----------

Run the following commands to bootstrap your environment.

    git clone path/to/git
    cd pelee_keras

## Setting up python
This project uses python 3.6. ``pip3`` should be installed. ``sudo`` might be used.

    cd source
    git clone https://github.com/kseniakhakalo/ssd_keras.git



**Setting up virtual environment**
(Alternatively one can just run pip3 install -r requirements.txt)

Install virtualenv:

    sudo apt install virtualenv


Create virtualenv from source folder:



    python3 -mvenv venv
    or
    virtualenv -p python3 venv

Install dependencies:

    venv/bin/pip install -r requirements.txt

Activate virtual environment:
    source venv/bin/activate

## Getting Data

To train model for example on VOC2012 dataset download it to data folder with this command

    get http://host.robots.ox.ac.uk/pascal/VOC/voc2012/VOCtrainval_11-May-2012.tar




## Jupyter + VirtualEnv

**Instructions for linux:**

 Activate virtual evironment

    $ source venv/bin/activate

From venv install jupyter

    $(venv) pip install jupyter

Run jupyter from venv

    $(venv) jupyter notebook

**Instructions for mac:**

From inside the environment install ipykernel using pip:

    venv/bin/pip install ipykernel

And now install a new kernel:  

    ipython kernel install --user --name=venv

At this point, you can start jupyter, create a new notebook and select the kernel that lives inside your environment.



## Jupyter + GIT

To add .ipynb files to git filtering should be applied. Filter sets "execution_count": null and "outputs": [].

1) Install jq package

for linux:

    sudo apt-get install jq

for mac:

    brew install jq


2) Add to .git/config (copy from raw README.md file to avoid problems with special symbols):

    [filter "clean_ipynb"]
        clean = jq --indent 1 --monochrome-output '. + if .metadata.git.suppress_outputs | not then { cells: [.cells[] | . + if .cell_type == \"code\" then { outputs: [], execution_count: null } else {} end ] } else {} end'
        smudge = cat

3) Add to .git/info/attributes  :

    *.ipynb  filter=clean_ipynb






## Directory Structure
----------

- **data**: All the data files are in handy/data. 
- **logs** : All log files are in handy/logs.
- **models** : All model files are in handy/models. 
- **source** : Source code in python. It has several subdirectories:
	



Contacts
----------
khakaloksenia@gmail.com
