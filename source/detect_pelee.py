from PIL import Image
import numpy as np
import cv2
from timeit import default_timer as timer
import argparse
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import matplotlib.pyplot as plt
from keras import backend as K
import logging
import sys, os
# need to add directory with ssd_keras to the path for imports to work
sys.path.append(os.path.abspath('./ssd_keras'))

from models.keras_ssd_pelee import build_pelee_model
from ssd_keras.data_generator.object_detection_2d_geometric_ops import Resize
from ssd_keras.data_generator.object_detection_2d_photometric_ops import ConvertTo3Channels
from ssd_keras.data_generator.object_detection_2d_misc_utils import apply_inverse_transforms

from utils import LoadFromFile, get_filetype
from settings import Config as cf
from visualization.utils import draw_bbox_with_label, get_colors

IMG_HEIGHT = 304  # Height of the input images
IMG_WIDTH = 304  # Width of the input images

# To run code on CPU uncomment the following:
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = ""

def detect_img(model, classes, display=False):
    '''
    While True, load image specified in the command line and run inference
    Parameters
    ----------
    model           :(keras.Model) trained Pelee model, built with inference mode on
    classes         :(list of str) list of class names
    display         :(bool) if True, display results of detections in real time

    Returns
    -------

    '''

    colors = get_colors(classes)
    # For the validation generator:
    convert_to_3_channels = ConvertTo3Channels()
    resize = Resize(height=IMG_HEIGHT, width=IMG_WIDTH)

    while True:
        img = input('Input image filename:')

        try:
            image = Image.open(img)
            image = np.asarray(image)
        except:
            print('Open Error! Try again!')
            continue

        data = convert_to_3_channels(image)
        data, resize_inv = resize(data, return_inverter=True)
        image_batch = np.expand_dims(data, axis=0)
        y_pred_decoded = model.predict(image_batch)

        # 4: Decode the raw prediction `y_pred`
        np.set_printoptions(precision=2, suppress=True, linewidth=90)
        print("Predicted boxes:\n")
        print('   class   conf xmin   ymin   xmax   ymax')
        print(y_pred_decoded)
        y_pred_decoded_inv = apply_inverse_transforms(y_pred_decoded, [[resize_inv]])
        if display:
            # Draw the predicted boxes in blue
            for box in y_pred_decoded_inv[0]:
                class_id = box[0]
                class_score = box[1]

                bb_handy = [box[2], box[3], box[4] - box[2], box[5] - box[3]]

                if class_score > 0.:
                    label = '{} {:.2f}'.format(classes[int(class_id) - 1], class_score)
                    image = draw_bbox_with_label(image, bb_handy, label=label, gt=False, color=colors[int(class_id)-1])

            plt.figure(figsize=(20, 12))
            plt.imshow(image)
            plt.show()


def detect_video(model, classes, video_path='0', output_path="", display=False):
    '''
    Read video and run inference frame by frame, optionally save results to an output file
    Parameters
    ----------
    model           :(keras.Model) trained Pelee model, built with inference mode on
    classes         :(list of str) list of class names
    video_path      :(str) path to input video, if '0': try to read from a webcam
    output_path     :(str) path to write output
    display         :(bool) if True, display results of detections in real time

    Returns
    -------

    '''

    colors = get_colors(classes)
    # For the validation generator:
    convert_to_3_channels = ConvertTo3Channels()
    resize = Resize(height=IMG_HEIGHT, width=IMG_WIDTH)


    if video_path in ['', '0']:
        vid = cv2.VideoCapture(0)
    else:
        vid = cv2.VideoCapture(video_path)
    if not vid.isOpened():
        raise IOError("Couldn't open webcam or video")
    video_FourCC = cv2.VideoWriter_fourcc(*'XVID')  # int(vid.get(cv2.CAP_PROP_FOURCC))
    video_fps = vid.get(cv2.CAP_PROP_FPS)
    video_size = (int(vid.get(cv2.CAP_PROP_FRAME_WIDTH)),
                  int(vid.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    isOutput = True if output_path != "" else False
    if isOutput:
        logging.info("!!! TYPE:{} {} {} {}".format(type(output_path),
                                                   type(video_FourCC),
                                                   type(video_fps),
                                                   type(video_size)))
        f_type = get_filetype(output_path)
        if f_type != '.avi':
            logging.warning('output_path was changed from {}'.format(output_path))
            output_path = output_path.replace(f_type, '.avi')
            logging.warning('to :{}'.format(output_path))
        out = cv2.VideoWriter(output_path, video_FourCC, video_fps, video_size)
    accum_time = 0
    curr_fps = 0
    fps = "FPS: ??"
    prev_time = timer()
    while True:
        return_value, frame = vid.read()

        frame = cv2.flip(frame, 1)
        if get_filetype(video_path) == '.3gp':
            frame = np.swapaxes(frame[..., ::-1, :], 1, 0)

        data = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        data = convert_to_3_channels(data)
        data, inverse_resize = resize(data, return_inverter=True)

        # except:
        #    print('Open Error! Try again!')
        #    continue
        image_batch = np.expand_dims(data, axis=0)
        y_pred_decoded = model.predict(image_batch)

        # 4: Decode the raw prediction `y_pred`
        np.set_printoptions(precision=2, suppress=True, linewidth=90)
        print("Predicted boxes:\n")
        print('   class   conf xmin   ymin   xmax   ymax')
        print(y_pred_decoded)
        # Convert the predictions for the original image.
        y_pred_decoded_inv = apply_inverse_transforms(y_pred_decoded, [[inverse_resize]])

        # Draw the predicted boxes in blue
        for box in y_pred_decoded_inv[0]:
            class_id = box[0]
            class_score = box[1]

            bb_handy = [box[2], box[3], box[4] - box[2], box[5] - box[3]]

            if class_score > 0.:
                label = '{} {:.2f}'.format(classes[int(class_id)-1], class_score)
                frame = draw_bbox_with_label(frame, bb_handy, label=label, gt=False, color=colors[int(class_id)-1])
        result = frame
        curr_time = timer()
        exec_time = curr_time - prev_time
        prev_time = curr_time
        accum_time = accum_time + exec_time
        curr_fps = curr_fps + 1
        if accum_time > 1:
            accum_time = accum_time - 1
            fps = "FPS: " + str(curr_fps)
            curr_fps = 0

        #result = cv2.cvtColor(result, cv2.COLOR_BGR2RGB)
        assert (isOutput or display), "Results are neither displayed nor saved anywhere. " \
                                      "Please pass --display flag or path to output with --output flag"

        if display:
            cv2.putText(result, text=fps, org=(3, 15), fontFace=cv2.FONT_HERSHEY_SIMPLEX,
                        fontScale=0.50, color=(255, 0, 0), thickness=2)
            cv2.namedWindow("result", cv2.WINDOW_NORMAL)
            cv2.imshow("result", result)
        if isOutput:
            out.write(result)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    K.close_session()

def get_model(num_classes, **kwargs):
    '''
    Parameters
    ----------
    num_classes   :(int) number of classes to detect
    kwargs        : arguments passed through command line

    Returns
    -------
    model         :(keras.Model) trained Pelee model, built with inference mode on
    '''

    img_channels = 3  # Number of color channels of the input images
    intensity_mean = 127.5  # Set this to your preference (maybe `None`). The current settings transform the input pixel values to the interval `[-1,1]`.
    intensity_range = 127.5  # Set this to your preference (maybe `None`). The current settings transform the input pixel values to the interval `[-1,1]`.
    scales = [0.08, 0.16, 0.32, 0.5, 0.64,
              0.96]  # An explicit list of anchor box scaling factors. If this is passed, it will override `min_scale` and `max_scale`.
    aspect_ratios = [0.2, 0.5, 1.0, 1.5, 2.0]  # The list of aspect ratios for the anchor boxes
    two_boxes_for_ar1 = True  # Whether or not you want to generate two anchor boxes for aspect ratio 1
    steps = None  # In case you'd like to set the step sizes for the anchor box grids manually; not recommended
    offsets = None  # In case you'd like to set the offsets for the anchor box grids manually; not recommended
    clip_boxes = False  # Whether or not to clip the anchor boxes to lie entirely within the image boundaries
    variances = [1.0, 1.0, 1.0, 1.0]  # The list of variances by which the encoded target coordinates are scaled
    normalize_coords = True  # Whether or not the model is supposed to use coordinates relative to the image size

    config = tf.ConfigProto()
    # config.gpu_options.allow_growth = True
    config.gpu_options.per_process_gpu_memory_fraction = 0.4  # determines the fraction of the overall GPU memory to use
    sess = tf.Session(config=config)
    set_session(sess)  # set this TensorFlow session as the default session for Keras
    logging.info('K.tensorflow_backend._get_available_gpus():{}'.format(K.tensorflow_backend._get_available_gpus()))

    model_path = kwargs.get('model_path')
    conf_thresh = kwargs.get('score', 0.5)

    K.clear_session()  # Clear previous models from memory.
    logging.info('Building a model')
    print('image_size=',(IMG_HEIGHT, IMG_WIDTH, img_channels),
            'n_classes=', num_classes,
            'scales=',scales,
            'aspect_ratios_global',aspect_ratios,
            'two_boxes_for_ar1=',two_boxes_for_ar1,
            'steps=', steps,
            'offsets=', offsets,
            'clip_boxes=', clip_boxes,
            'variances=', variances,
            'normalize_coords=', normalize_coords,
            'subtract_mean=', intensity_mean,
            'divide_by_stddev=', intensity_range,
            'confidence_thresh=', conf_thresh)
    model = build_pelee_model(image_size=(IMG_HEIGHT, IMG_WIDTH, img_channels),
                              n_classes=num_classes,
                              mode='inference_fast',
                              l2_regularization=0.0005,
                              scales=scales,
                              aspect_ratios_global=aspect_ratios,
                              aspect_ratios_per_layer=None,
                              two_boxes_for_ar1=two_boxes_for_ar1,
                              steps=steps,
                              offsets=offsets,
                              clip_boxes=clip_boxes,
                              variances=variances,
                              normalize_coords=normalize_coords,
                              subtract_mean=intensity_mean,
                              divide_by_stddev=intensity_range,
                              confidence_thresh=conf_thresh)
    logging.info('Loading weights')
    model.load_weights(model_path)

    return model


FLAGS = None

if __name__ == '__main__':

    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    '''
    Command line options
    '''
    parser.add_argument('--file', type=open, action=LoadFromFile)

    parser.add_argument(
        '--model_path', type=str,
        help='path to model weight file, default '
    )

    parser.add_argument(
        '--classes_path', type=str, default=cf.ROOT_DIR + '/model_data/handy_classes_with_9_gestures_morpho_v2.txt',
        help='path to class definitions, default '
    )

    parser.add_argument(
        '--image', default=False, action="store_true",
        help='Image detection mode, will ignore all positional arguments'
    )
    '''
    Command line positional arguments -- for video detection mode
    '''
    parser.add_argument(
        "--input", nargs='?', type=str, required=False, default='0',
        help="Video input path"
    )

    parser.add_argument(
        "--output", nargs='?', type=str, default="",
        help="[Optional] Video output path"
    )

    parser.add_argument(
        '--score', type=float,
        help='Confidence threshold to use, default ' + str(0.5)
    )

    parser.add_argument(
        '--display', default=False, action="store_true",
        help='Display detection results for video in real time'
    )

    FLAGS = parser.parse_args()
    print(FLAGS)

    classes_path = FLAGS.classes_path
    classes = ['aeroplane', 'bicycle', 'bird', 'boat',
               'bottle', 'bus', 'car', 'cat',
               'chair', 'cow', 'diningtable', 'dog',
               'horse', 'motorbike', 'person', 'pottedplant',
               'sheep', 'sofa', 'train', 'tvmonitor']

    num_classes = len(classes)
    print('num_classes:', num_classes)
    model = get_model(num_classes, **vars(FLAGS))

    if FLAGS.image:
        """
        Image detection mode, disregard any remaining command line arguments
        """
        print("Image detection mode")
        if "input" in FLAGS:
            print(" Ignoring remaining command line arguments: " + FLAGS.input + "," + FLAGS.output)
        detect_img(model, classes, display=FLAGS.display)
    elif "input" in FLAGS:
        detect_video(model, classes, video_path=FLAGS.input, output_path=FLAGS.output, display=FLAGS.display)
    else:
        print("Must specify at least video_input_path.  See usage with --help.")
