"""
Author         : Kseniia Khakalo
Contact        : khakaloksenia@gmail.com
Created        : 22/10/2019
Latest Version : 22/10/2019
Description    : File description
"""
import argparse
import os


class LoadFromFile (argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        with values as f:
            parser.parse_args(f.read().split(), namespace)


def get_filetype(path):
    '''
    Extracts filename from a path
        path          : path to a directory or file (str)
    Returns : filetype (str)
    Example : get_filetype('./home/abc/pip_list.txt')   will return: '.txt'
    '''

    base = os.path.basename(path)
    return os.path.splitext(base)[1]