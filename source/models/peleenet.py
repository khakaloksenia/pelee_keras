"""
Author         : Kseniia Khakalo
Contact        : khakaloksenia@gmai.com
Created        : 22/10/2019
Latest Version : 22/10/2019
Description    : Define Pelee base model and PeleeNet architectures,
                for more details see [https://arxiv.org/abs/1804.06882]
"""
from __future__ import print_function

import keras.backend as K
from keras.models import Model
from keras.layers import Activation
from keras.layers.convolutional import Conv2D
from keras.layers.pooling import AveragePooling2D, MaxPooling2D
from keras.layers.merge import concatenate
from keras.layers.normalization import BatchNormalization



def conv_block(ip, nb_filter, name='', use_bn=True, use_relu=True, kernel_size=3, stride=1, padding='same'):
    """Apply Conv2D, BatchNorm, and Relu

    Parameters
    ----------
    ip              :(keras.Layer) input layer to add conv block to
    nb_filter       :(int) number of filters
    name            :(str)  block name
    use_bn          :(bool) if True: use BatchNormalization
    use_relu        :(bool) if True, use ReLu activation function
    kernel_size     :(int) kernel size
    stride          :(int) stride size
    padding         :(str) padding type

    Returns
    -------
    output keras.Layer
    """
    concat_axis = 1 if K.image_data_format() == 'channels_first' else -1

    x = Conv2D(nb_filter, (kernel_size, kernel_size), strides=(stride, stride), name=name,
               kernel_initializer='he_normal', padding=padding, use_bias=False)(ip)
    if use_bn: x = BatchNormalization(axis=concat_axis, name='{}/bn'.format(name), epsilon=1.1e-5)(x)

    if use_relu: x = Activation('relu', name='{}/bn/relu'.format(name))(x)

    return x


def dense_block(x, nb_layers, growth_rate, name,
                bottleneck_width=4):
    """Build a dense_block

    Parameters
    ----------
    x                   : (keras.Layer) input layer to add dense block to
    nb_layers           : (int) number of dense layers in the block
    growth_rate         : (int) effective number of filters for each dense layer
    name                : (str) block name
    bottleneck_width    : (int) bottleneck width for transition layer to reduce input size for the dense layer

    Returns
    -------
    output keras.Layer
    """
    concat_axis = 1 if K.image_data_format() == 'channels_first' else -1

    growth_rate = int(growth_rate / 2)

    for i in range(nb_layers):
        base_name = '{}_{}'.format(name, i + 1)

        inter_channel = int(growth_rate * bottleneck_width / 4) * 4

        cb1 = conv_block(x, inter_channel, name='{}/branch1a'.format(base_name), kernel_size=1, stride=1)
        cb1 = conv_block(cb1, growth_rate, name='{}/branch1b'.format(base_name), kernel_size=3, stride=1)

        cb2 = conv_block(x, inter_channel, name='{}/branch2a'.format(base_name), kernel_size=1, stride=1)
        cb2 = conv_block(cb2, growth_rate, name='{}/branch2b'.format(base_name), kernel_size=3, stride=1)
        cb2 = conv_block(cb2, growth_rate, name='{}/branch2c'.format(base_name), kernel_size=3, stride=1)

        concate_name = '{}/concat'.format(base_name)
        x = concatenate([x, cb1, cb2], axis=concat_axis, name=concate_name)

    return x


def transition_block(from_layer, num_filter, name='', with_pooling=True):
    """Build transition layer to decrease input depth the data

    Parameters
    ----------
    from_layer          : (keras.Layer) input layer to add transition block to
    num_filter          : (int) number of filters in conv layer
    name                : (str) name of the block
    with_pooling        : (bool) if True, do AveragePooling

    Returns
    -------
    output keras.Layer
    """

    conv = conv_block(from_layer, num_filter, name=name, kernel_size=1, stride=1)

    if with_pooling:
        pool_name = '{}/pool'.format(name)
        pooling = AveragePooling2D(pool_size=(2, 2), strides=(2, 2), name=pool_name)(conv)
        from_layer = pooling
    else:
        from_layer = conv

    return from_layer


def stem_block(from_layer, num_init_features):
    """
    Build stem block
    Parameters
    ----------
    from_layer          : (keras.Layer) input layer to add stem block to
    num_init_features   : (int) number of filters in conv layer

    Returns
    -------
    output keras.Layer

    """
    concat_axis = 1 if K.image_data_format() == 'channels_first' else -1
    stem1 = conv_block(from_layer, num_init_features, name='stem1', kernel_size=3, stride=2)

    stem2 = conv_block(stem1, int(num_init_features / 2), name='stem2a', kernel_size=1, stride=1)

    stem2 = conv_block(stem2, num_init_features, name='stem2b', kernel_size=3, stride=2)

    stem1 = MaxPooling2D(name='stem/pool', pool_size=(2, 2), strides=(2, 2))(stem1)

    concate = concatenate([stem1, stem2], name='stem/concat', axis=concat_axis)

    stem3 = conv_block(concate, num_init_features, name='stem3', kernel_size=1, stride=1)

    return stem3


def add_extra_layers_pelee(from_layer, from_layer_name='stage4_tb', use_batchnorm=True, prefix='ext/fe'):
    """
    Add extra layers to base model
    Parameters
    ----------
    from_layer          : (keras.Layer) input layer to add extra layers to
    from_layer_name     : (str) name of input layer
    use_batchnorm       : (bool) if True, use BatchNorm
    prefix              : (str) prefix to add to extra layers' names

    Returns
    -------
    output keras.Layer

    """

    use_relu = True
    # Add additional convolutional layers.

    # stage2_tb: 38 x 38 x 256
    # stage3_tb: 19 x 19 x 512
    # stage4_tb: 10 x 10 x 704

    # 5 x 5
    out_layer_name = '{}/{}1_1'.format(from_layer_name, prefix)
    out_layer = conv_block(from_layer, 256, name=out_layer_name, use_bn=use_batchnorm, use_relu=use_relu, kernel_size=1, stride=1)

    from_layer = out_layer
    out_layer_name = '{}1_2'.format(prefix)
    out_layer = conv_block(from_layer, 256, name=out_layer_name, use_bn=use_batchnorm, use_relu=use_relu, kernel_size=3, stride=2)


    # 3 x 3
    from_layer = out_layer
    out_layer_name = '{}2_1'.format(prefix)
    out_layer = conv_block(from_layer, 128, name=out_layer_name, use_bn=use_batchnorm, use_relu=use_relu,
                           kernel_size=1, stride=1)

    from_layer = out_layer
    out_layer_name = '{}2_2'.format(prefix)
    out_layer = conv_block(from_layer, 256, name=out_layer_name, use_bn=use_batchnorm, use_relu=use_relu,
                           kernel_size=3, stride=1, padding='valid')

    # 1 x 1
    from_layer = out_layer
    out_layer_name = '{}3_1'.format(prefix)
    out_layer = conv_block(from_layer, 128, name=out_layer_name, use_bn=use_batchnorm, use_relu=use_relu,
                           kernel_size=1, stride=1)

    from_layer = out_layer
    out_layer_name = '{}3_2'.format(prefix)

    out_layer = conv_block(from_layer, 256, name=out_layer_name, use_bn=use_batchnorm, use_relu=use_relu,
                           kernel_size=3, stride=1, padding='valid')

    return out_layer

def create_PeleeNetBody(input, growth_rate=32, block_config=[3, 4, 8, 6], bottleneck_width=[1, 2, 4, 4],
                        num_init_features=32, init_kernel_size=3, use_stem_block=True):
    """
    Build graph for PeleeNet
    Parameters
    ----------
    input               : input layer to start from
    growth_rate         :(int) number of filters to add in each dense layer
    block_config        :(list of int) number of dense layers in each dense block
    bottleneck_width    :(list of int) bottleneck width for each dense block
    num_init_features   :(int) initial number of filters
    init_kernel_size    :(int) kernel size for firs convolutional layer
    use_stem_block      :(bool) if True, use stem block

    Returns
    -------
    output keras.Layer

    """

    # Initial convolution
    if use_stem_block:
        from_layer = stem_block(input, num_init_features)

    else:
        out_layer = conv_block(input, num_init_features, name='conv1', kernel_size=init_kernel_size, stride=2)
        from_layer = MaxPooling2D(out_layer, pool_size=(2,2), strides=(2,2))

    total_filter = num_init_features
    if type(bottleneck_width) is list:
        bottleneck_widths = bottleneck_width
    else:
        bottleneck_widths = [bottleneck_width] * 4

    for idx, num_layers in enumerate(block_config):

        from_layer = dense_block(from_layer, num_layers, growth_rate, name='stage{}'.format(idx + 1),
                                 bottleneck_width=bottleneck_widths[idx])
        total_filter += growth_rate * num_layers

        if idx == len(block_config) - 1:
            with_pooling = False
        else:
            with_pooling = True

        from_layer = transition_block(from_layer, total_filter, name='stage{}_tb'.format(idx + 1),
                                      with_pooling=with_pooling)

    return from_layer


def get_PeleeNetBody(inputs, from_layer):
    """Get PeleeNet model

    Parameters
    ----------
    inputs      :(keras.Layer) input layer of the model
    from_layer  :(keras.Layer) layer, to which add PeleeBody layers

    Returns
    -------
    (keras.Model) PeleeNet model
    """

    x = create_PeleeNetBody(from_layer)
    # Create model.
    model = Model(inputs, x, name='PeleeNet')
    return model


def get_PeleeBody(inputs, from_layer):
    """Get PeleeBody model, which is PeleeNet + extra conv layers

    Parameters
    ----------
    inputs      :(keras.Layer) input layer of the model
    from_layer  :(keras.Layer) layer, to which add PeleeBody layers

    Returns
    -------
    (keras.Model) PeleeBody model
    """

    x = create_PeleeNetBody(from_layer)
    x = add_extra_layers_pelee(x)
    # Create model.
    model = Model(inputs, x, name='PeleeBody')
    return model
