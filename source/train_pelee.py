"""
Retrain the Pelee model for your own dataset.
"""
import argparse
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session
import json
from os.path import join
from os.path import exists
from os import makedirs
from keras.optimizers import Adam
from keras.callbacks import ModelCheckpoint, EarlyStopping, ReduceLROnPlateau
from keras import backend as K
import logging
import sys, os
# need to add directory with ssd_keras to the path for imports to work
sys.path.append(os.path.abspath('./ssd_keras'))

from models.keras_ssd_pelee import build_pelee_model
from ssd_keras.keras_loss_function.keras_ssd_loss import SSDLoss
from ssd_keras.ssd_encoder_decoder.ssd_input_encoder import SSDInputEncoder
from ssd_keras.data_generator.object_detection_2d_data_generator import DataGenerator
from ssd_keras.data_generator.data_augmentation_chain_constant_input_size import DataAugmentationConstantInputSize
from ssd_keras.data_generator.object_detection_2d_geometric_ops import Resize
from ssd_keras.data_generator.object_detection_2d_photometric_ops import ConvertTo3Channels, ConvertColor
from utils.callbacks import TrainValTensorBoard
from utils import LoadFromFile
from settings import Config as cf

#os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
#os.environ["CUDA_VISIBLE_DEVICES"] = ""


def _main(**kwargs):

    # Uncomment the following to set gpu_memory_fraction
    # if you want to train several models simultaneously
    #config = tf.ConfigProto()
    #config.gpu_options.allow_growth = True
    #config.gpu_options.per_process_gpu_memory_fraction = 0.3 # determines the fraction of the overall GPU memory to use
    #sess = tf.Session(config=config)
    #set_session(sess)  # set this TensorFlow session as the default session for Keras

    print('K.tensorflow_backend._get_available_gpus():', K.tensorflow_backend._get_available_gpus())

    log_dir = kwargs.get('log_dir', '../logs/log1/')
    model_path = kwargs.get('model_path')

    img_height = 304  # Height of the input images
    img_width = 304  # Width of the input images
    img_channels = 3  # Number of color channels of the input images
    intensity_mean = 127.5  # Set this to your preference (maybe `None`). The current settings transform the input pixel values to the interval `[-1,1]`.
    intensity_range = 127.5  # Set this to your preference (maybe `None`). The current settings transform the input pixel values to the interval `[-1,1]`.
    scales = [0.08, 0.16, 0.32, 0.5, 0.64, 0.96]  # An explicit list of anchor box scaling factors. If this is passed, it will override `min_scale` and `max_scale`.
    aspect_ratios = [0.2, 0.5, 1.0, 1.5, 2.0]  # The list of aspect ratios for the anchor boxes
    two_boxes_for_ar1 = True  # Whether or not you want to generate two anchor boxes for aspect ratio 1
    steps = None  # In case you'd like to set the step sizes for the anchor box grids manually; not recommended
    offsets = None  # In case you'd like to set the offsets for the anchor box grids manually; not recommended
    clip_boxes = False  # Whether or not to clip the anchor boxes to lie entirely within the image boundaries
    variances = [1.0, 1.0, 1.0, 1.0]  # The list of variances by which the encoded target coordinates are scaled
    normalize_coords = True  # Whether or not the model is supposed to use coordinates relative to the image size


    # Save params to json file
    if not exists(log_dir):
        makedirs(log_dir)
    with open(join(log_dir, 'config.json'), 'w') as outfile:
        json.dump(kwargs, outfile)

    classes = ['aeroplane', 'bicycle', 'bird', 'boat',
               'bottle', 'bus', 'car', 'cat',
               'chair', 'cow', 'diningtable', 'dog',
               'horse', 'motorbike', 'person', 'pottedplant',
               'sheep', 'sofa', 'train', 'tvmonitor']

    num_classes = len(classes) # number of positive classes

    # 1: Build the Keras model

    K.clear_session()  # Clear previous models from memory.

    model = build_pelee_model(image_size=(img_height, img_width, img_channels),
                              n_classes=num_classes,
                              mode='training',
                              l2_regularization=0.0005,
                              scales=scales,
                              aspect_ratios_global=aspect_ratios,
                              aspect_ratios_per_layer=None,
                              two_boxes_for_ar1=two_boxes_for_ar1,
                              steps=steps,
                              offsets=offsets,
                              clip_boxes=clip_boxes,
                              variances=variances,
                              normalize_coords=normalize_coords,
                              subtract_mean=intensity_mean,
                              divide_by_stddev=intensity_range)

    # 2: Instantiate an Adam optimizer and the SSD loss function and compile the model
    learning_rate = 0.001
    adam = Adam(lr=learning_rate, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
    logging.info('Adam optimizer is in use, lr = {}'.format(learning_rate))

    ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
    model.compile(optimizer=adam, loss=ssd_loss.compute_loss)

    # 3: Optional: Load weights
    if model_path:
        model.load_weights(model_path, by_name=True)

    # 4: Instantiate two `DataGenerator` objects: One for training, one for validation.

    # Optional: If you have enough memory, consider loading the images into memory.

    train_dataset = DataGenerator(load_images_into_memory=False, hdf5_dataset_path=None)
    val_dataset = DataGenerator(load_images_into_memory=False, hdf5_dataset_path=None)

    # 5: Parse the image and label lists for the training and validation datasets.

    # The directory that contain the images.
    VOC_2012_images_dir = cf.DATA_DIR+'/VOCdevkit/VOC2012/JPEGImages/'

    # The directory that contain the annotations.
    VOC_2012_annotations_dir = cf.DATA_DIR+'/VOCdevkit/VOC2012/Annotations/'

    # The paths to the image sets.
    VOC_2012_train_image_set_filename = cf.DATA_DIR+'/VOCdevkit/VOC2012/ImageSets/Main/train.txt'
    VOC_2012_val_image_set_filename = cf.DATA_DIR+'/VOCdevkit/VOC2012/ImageSets/Main/val.txt'

    # The XML parser needs to now what object class names to look for and in which order
    # to map them to integers.

    train_dataset.parse_xml(images_dirs=[VOC_2012_images_dir],
                            image_set_filenames=[VOC_2012_train_image_set_filename],
                            annotations_dirs=[VOC_2012_annotations_dir],
                            classes=classes,
                            include_classes='all',
                            exclude_truncated=False,
                            exclude_difficult=False,
                            ret=False)

    val_dataset.parse_xml(images_dirs=[VOC_2012_images_dir],
                          image_set_filenames=[VOC_2012_val_image_set_filename],
                          annotations_dirs=[VOC_2012_annotations_dir],
                          classes=classes,
                          include_classes='all',
                          exclude_truncated=False,
                          exclude_difficult=True,
                          ret=False)

    # Get the number of samples in the training and validations datasets.
    train_dataset_size = train_dataset.get_dataset_size()
    val_dataset_size = val_dataset.get_dataset_size()

    logging.info("Number of images in the training dataset:\t{:>6}".format(train_dataset_size))
    logging.info("Number of images in the validation dataset:\t{:>6}".format(val_dataset_size))

    # 6: Set the batch size.

    batch_size = 16
    logging.info('Batch size = {}'.format(batch_size))

    # Define transformations for the validation generator:
    convert_to_3_channels = ConvertTo3Channels()
    resize = Resize(height=img_height, width=img_width)
    logging.info('Model image shape = {}'.format((img_height, img_width)))

    # 7: Define the image processing chain.

    data_augmentation_chain = DataAugmentationConstantInputSize(random_brightness=(-48, 48, 0.5),
                                                                random_contrast=(0.5, 1.8, 0.5),
                                                                random_saturation=(0.5, 1.8, 0.5),
                                                                random_hue=(18, 0.5),
                                                                random_flip=0.5,
                                                                random_translate=((0.03, 0.5), (0.03, 0.5), 0.0),
                                                                random_scale=(0.5, 2.0, 0.5),
                                                                n_trials_max=3,
                                                                clip_boxes=True,
                                                                overlap_criterion='area',
                                                                bounds_box_filter=(0.3, 1.0),
                                                                bounds_validator=(0.5, 1.0),
                                                                n_boxes_min=1,
                                                                background=(0, 0, 0))

    # 8: Instantiate an encoder that can encode ground truth labels into the format needed by the SSD loss function.

    # The encoder constructor needs the spatial dimensions of the model's predictor layers to create the anchor boxes.
    predictor_sizes = [model.get_layer('classes0').output_shape[1:3],
                       model.get_layer('classes1').output_shape[1:3],
                       model.get_layer('classes2').output_shape[1:3],
                       model.get_layer('classes3').output_shape[1:3],
                       model.get_layer('classes4').output_shape[1:3]]

    ssd_input_encoder = SSDInputEncoder(img_height=img_height,
                                        img_width=img_width,
                                        n_classes=len(classes),
                                        predictor_sizes=predictor_sizes,
                                        scales=scales,
                                        aspect_ratios_global=aspect_ratios,
                                        two_boxes_for_ar1=two_boxes_for_ar1,
                                        steps=steps,
                                        offsets=offsets,
                                        clip_boxes=clip_boxes,
                                        variances=variances,
                                        matching_type='multi',
                                        pos_iou_threshold=0.5,
                                        neg_iou_limit=0.3,
                                        normalize_coords=normalize_coords)

    # 9. Create the generator handles that will be passed to Keras' `fit_generator()` function.

    train_generator = train_dataset.generate(batch_size=batch_size,
                                             shuffle=True,
                                             transformations=[convert_to_3_channels, resize,
                                                              data_augmentation_chain],
                                             label_encoder=ssd_input_encoder,
                                             returns={'processed_images',
                                                      'encoded_labels'},
                                             keep_images_without_gt=False)

    val_generator = val_dataset.generate(batch_size=batch_size,
                                         shuffle=False,
                                         transformations=[convert_to_3_channels, resize],
                                         label_encoder=ssd_input_encoder,
                                         returns={'processed_images',
                                                  'encoded_labels'},
                                         keep_images_without_gt=False)

    # Define model callbacks.
    checkpoint = ModelCheckpoint(
                                filepath=join(log_dir, 'ssd_pelee_epoch_{epoch:02d}_loss_{loss:.4f}_val_loss_{val_loss:.4f}.h5'),
                                monitor='val_loss',
                                verbose=1,
                                save_best_only=True,
                                save_weights_only=False,
                                mode='auto',
                                period=1)

    logs = TrainValTensorBoard(log_dir=log_dir)

    early_stopping = EarlyStopping(monitor='val_loss',
                                   min_delta=0.0,
                                   patience=20,
                                   verbose=1)

    reduce_lr = ReduceLROnPlateau(monitor='val_loss',
                                 factor=0.2,
                                 patience=8,
                                 verbose=1,
                                 epsilon=0.001,
                                 cooldown=0,
                                 min_lr=0.000001)

    callbacks = [checkpoint,
                 logs,
                 early_stopping,
                 reduce_lr]

    # If you're resuming a previous training, set `initial_epoch` and `final_epoch` accordingly.
    initial_epoch = 0
    final_epoch = 500

    steps_per_epoch = max(1, train_dataset_size // batch_size)
    validation_steps = max(1, val_dataset_size // batch_size)

    history = model.fit_generator(generator=train_generator,
                                  steps_per_epoch=steps_per_epoch,
                                  epochs=final_epoch,
                                  callbacks=callbacks,
                                  validation_data=val_generator,
                                  validation_steps=validation_steps,
                                  initial_epoch=initial_epoch)

    model.save_weights(join(log_dir, 'trained_weights_final.h5'))

if __name__ == '__main__':

    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    '''
    Command line options
    '''

    parser.add_argument(
        '--model_path', type=str,
        help='path to model weight file'
    )

    parser.add_argument(
        '--log_dir', type=str,
        help='path to log directory to save modes callbacks '
    )

    parser.add_argument('--file', type=open, action=LoadFromFile)


    FLAGS = parser.parse_args()
    print(FLAGS)
    _main(**vars(FLAGS))